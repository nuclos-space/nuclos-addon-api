
/**
 * the SortModel contains information for sorting a result list or subform
 */
export declare abstract class ISortModel {

	constructor(columns: SortAttribute[]);

	/**
	 * column sorting definitions
	 * @return {SortAttribute[]}
	 */
	getColumns(): SortAttribute[];

}

/**
 * sort direction
 */
export type SortDirection = 'asc' | 'desc';

/**
 * describes the attributes sorting
 */
export interface SortAttribute {
	/**
	 * column id
	 */
	colId: string;

	/**
	 * sort direction
	 */
	sort: SortDirection;
}
