import { Observable } from 'rxjs';
import { ISortModel } from './sorting';


/**
 * represents a single data entry
 */
export interface IEntityObject {

	/**
	 * entity object id
	 * @return {number} undefined if not saved
	 */
	getId(): number | undefined;

	/**
	 * get attribute value
	 * @param {string} attributeId
	 * @return {any} the attributes value
	 */
	getAttribute(attributeId: string): any;

	/**
	 * Sets the value for the attribute with the given ID
	 * and marks this EO as modified.
	 * This method does nothing if the old value equals the new value.
	 *
	 * @param attributeId
	 * @param value
	 */
	setAttribute(attributeId: string, value: any): void;

	/**
	 * get the list of dependent entries (subform entries)
	 * @param {string} attributeId
	 * @param {ISortModel} sortModel
	 * @return {IEntityObjectDependents}
	 */
	getDependents(referenceAttributeFqn: string, sortModel?: ISortModel): IEntityObjectDependents;

	/**
	 * @return {boolean} true if the EntityObject is not persisted
	 */
	isNew(): boolean;

	getEntityClassId(): string;

	/**
	 * Returns the temporary ID of a new, unsaved EO.
	 *
	 * @return {string}
	 */
	getTemporaryId(): string | undefined;

	/**
	 * Saves this EntityObject.
	 * @return {Observable<IEntityObject>} which returns the updated EO after save
	 */
	save(): Observable<IEntityObject>;

	/**
	 * An error that occurred e.g. during a failed generation attempt.
	 */
	getError(): string | undefined;

	isMarkedAsDeleted(): boolean;

	/**
	 * Sets this EO as globally selected (for display/editing).
	 */
	select(): void

	/**
	 * Determines if this EO is dirty, i.e. it has unsaved changes.
	 */
	isDirty(): boolean;

	/**
	 * Re-loads the data for this EO from the server.
	 *
	 */
	reload(): Observable<IEntityObject>;

	/**
	 * Resets this EO to the state it had last before becoming dirty.
	 * Also resets all dependents.
	 */
	reset(): void;

	getMandatorId(): string;

	/**
	 * Determines if this EO is deletable.
	 * New objects are not deletable (only cancelable).
	 */
	canDelete(): boolean;

	canWrite(): boolean;

	isAttributeReadable(attributeName: string): boolean;
	isAttributeWritable(attributeName: string): boolean;

	getCreatedBy(): string | undefined;
	getCreatedAt(): Date | undefined;
	getChangedBy(): string | undefined;
	getChangedAt(): Date | undefined;
}



/**
 * subform entity object
 */
export interface ISubEntityObject extends IEntityObject {

	/**
	 * Clones this Sub-EO.
	 * See {@link EntityObject#clone()}
	 *
	 * @returns {SubEntityObject}
	 */
	clone(): ISubEntityObject;

	/**
	 * Wether this Sub-EO is selected (e.g. a selected row in a subform).
	 */
	isSelected(): boolean;

	/**
	 * parent EO
	 * @returns {IEntityObject}
	 */
	getParent(): IEntityObject;
}

/**
 * provides subform entity objects
 */
export interface IEntityObjectDependents {

	/**
	 * access subform entity objects
	 */
	asObservable(): Observable<ISubEntityObject[] | undefined>;

	/**
	 * add a list of dependents
	 */
	addAll(dependents: ISubEntityObject[]): void;

	/**
	 * remove a list of dependents
	 */
	removeAll(dependents: ISubEntityObject[]): void;

	/**
	 * Loads all dependents for the given attribute FQN.
	 */
	loadIfEmpty(sortModel?: ISortModel): void;

	/**
	 * The currently loaded dependents.
	 */
	current(): ISubEntityObject[] | undefined;

	clear(): void;

	set(subEos: ISubEntityObject[]): void;

	isEmpty(): boolean;

	isLoading(): boolean;

	reload(sortModel?: ISortModel): void;
}
