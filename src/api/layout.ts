import { Observable } from 'rxjs';
import { IEntityObject, ISubEntityObject } from './entity-object';
import { AddonContext } from './addon-api';
import { InjectionToken } from '@angular/core';

// TODO: The class LayoutContext could be injected directly - no need for an extra InjectionToken
export const LAYOUT_CONTEXT = new InjectionToken<LayoutContext>('layout-context');

/**
 * API for accessing EntityObject data from an layout addon component
 */
export declare abstract class LayoutContext extends AddonContext {

	/**
	 * instantiate a new subform entity object
	 * @param {IEntityObject} parentEo
	 * @param {string} referenceAttributeId
	 * @return {Observable<ISubEntityObject>}
	 */
	newDependentEo(
		parentEo: IEntityObject,
		referenceAttributeId: string
	): Observable<ISubEntityObject>;

}
