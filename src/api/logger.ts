export enum LogLevel {
	ALL,
	DEBUG,
	INFO,
	WARN,
	ERROR,
	OFF,
}

// Define the interface that all loggers must implement.
export interface ILogger {
	getLogLevel(): LogLevel;
	setLogLevel(logLevel: LogLevel): void;

	assert(...args: any[]): void;
	error(...args: any[]): void;
	group(...args: any[]): void;
	groupEnd(...args: any[]): void;
	info(...args: any[]): void;
	log(...args: any[]): void;
	warn(...args: any[]): void;
	debug(...args: any[]): void;
}

// Set up the default logger. The default logger doesn't actually log anything; but, it
// provides the Dependency-Injection (DI) token that the rest of the application can use
// for dependency resolution. Each platform can then override this with a platform-
// specific logger implementation, like the ConsoleLogService (below).
export class Logger implements ILogger {
	static instance: ILogger = new Logger();

	assert(...args: any[]): void {
		// ... the default logger does no work.
	}

	error(...args: any[]): void {
		// ... the default logger does no work.
	}

	group(...args: any[]): void {
		// ... the default logger does no work.
	}

	groupEnd(...args: any[]): void {
		// ... the default logger does no work.
	}

	info(...args: any[]): void {
		// ... the default logger does no work.
	}

	log(...args: any[]): void {
		// ... the default logger does no work.
	}

	warn(...args: any[]): void {
		// ... the default logger does no work.
	}

	debug(...args: any[]): void {
		// ... the default logger does no work.
	}

	trace(...args: any[]): void {
		// ... the default logger does no work.
	}

	getLogLevel(): LogLevel {
		return LogLevel.OFF;
	}

	setLogLevel(logLevel: LogLevel): void {
	}
}
